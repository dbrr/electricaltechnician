# Intro
If you have stumbled upon this, it is designed to help with studying for the Assessment day in the Australian Defence Force (ADF)

*Note: This guide is more for NAVY applicants due to entering into the Navy myself*  
*Where required, interchange or disregard information regarding your entry.*  
*Example: A candidate entering into the Army/Airforce would not be required to complete the RANST Swim Test*

# What to study for

1. Motivation
*Motivation* is a major point which will be evaulated in detail by each of the interviewers
	- Why do you wish to be in the ADF?
	- Why do you wish to join the Service you are applying for (Navy)
	- Why do you want to be ....? 
	- What do you think are some of the challenges you will face @ Basic Training?
	- How will you overcome these challenges
	- **Challenges you'll face in:**
		- Navy
		- Training
		- Your job
		- Deployment
	- You could possibly be deployed into a combat environment 
		- How does this make you feel?
		- Challenges you could face
		- Can you think of an example here? 
2. Navy Values [here](http://www.navy.gov.au/navy-values-and-signature-behaviours#values)  
*Values guide our behaviour.*
	- Know the 5 key values
	- *Explain what these values mean to you in your own words*
		- Honor
		- Honesty
		- Courage
		- Integrity
		- Loyalty
3. Your Job **Important**
	- General overview
	- Daily tasks
	- What do you think your day-to-day is like?
	- **IMPS** (Initial Minimum Period of Service)
	- What are some of the responsibilities of the Service you are applying for?
	- What level of security clearance do you need? [link](https://navy.defencejobs.gov.au/-/media/DFR/Files/Navy-SECURITY-CLEARANCE.pdf)
4. Job Training **Important**
	- Initial Military Training (IMT) [link](http://www.navy.gov.au/join-navy/recruit-school)
		- Duration?
		- Location?
		- Content? [link](http://www.navy.gov.au/join-navy/recruit-school/course-content)
	- Initial Employment Training (IET)
		- Duration?
		- Location?
		- Content
	- Any additional training?
		- Know where/what/why 
5. Medical & Fitness 
	- [Fitness standards](http://www.defencejobs.gov.au/fitness/)
	- [Medical Process](http://content.defencejobs.gov.au/pdf/triservice/DFT_Document_MedicalProcess.pdf)
	- [Standards when your in](http://www.defencejobs.gov.au/content/pdf/army/Army_Physical_Continuum_Information.pdf)
	- **Navy applicants** Royal Australian Swim Test (RANST) [link](http://www.navy.gov.au/join-navy/recruit-school/fitness)
	- Describe your current training program
	- When did you last attempt all elements of a Pre-Entry Fitness Assessment (PFA) at the same time?
6. ADF Policies and Conditions
	- What are the ADF Policies and Conditions? Understand & be prepared to describe them.
		- Drugs & Alcohol - Non Medical Use of Drugs (NMUD)
		- Defence Force Discipline Act (DFDA)
		- Equity & Diversity (E&D)
		- IMPS
		- What is Unrestricted Service?
		- Your understanding of a Combat Role
7. Understanding the ADF
	- Ships in the Navy (name them)
	- Current operations in the Navy (name them) [link](http://www.navy.gov.au/navy-today/operations)
		- Domestic/local
		- Overseas
	- Current exercises in the Navy (name them) [link](http://www.navy.gov.au/navy-today/exercises)

# From Navy.gov

## About the NAVY
Defending Australia and its Interests

- 16 Naval Establishments
- 20 000+ Sailors & Officers, including 6 000+ Reserves
- 100+ Vessels & Aircraft, from submarines to helicopters

## Values
What do these values mean to me?  

**Honor**  
Honor is the fundamental value. To demonstrate honor, it demands honesty, courage, integrity & loyalty consistently.  
- To not be disposed to cheat or defraud

**Honesty**  
To always be truthful, knowing and doing what is right for the Navy and ourselves.
- Being honest to yourself
- Accountability

**Courage**  
Courage is the strength of character to do what is right in the face of personal adversity, danger or threat.
- Confronting fear and over comming it

**Integrity**  
Integrity is the display of truth, honesty and fairness that gains respect and trust from others.
- Demonstrating the principles you believe in

**Loyalty**  
Loyalty is being committed to each other and our duty of service to Australia.
- Commitment Standing beside you fel to your brother


## Current Year's Exercises

### KAKADU 2018
Darwin & northern Australia

- Australia's largest maritime exercise  
- Aims to foster and strengthen effective security

### RIMPAC 2018
Hawaii & Southern California, USA

`Theme of RIMPAC 2018 is Capable, Adaptive, Partners.`

- A combined exercise to strengthen maritime partnerships
- Enhance interoperability
- Improve the readiness of participating forces for potential operations

Includes:
- 25 countries
- 47 surface ships
- 5 Submarines
- 200+ aircraft
- 25 000+ personnel

## IPE (Indo-PACIFIC ENDEAVOUR) 2018
Indo-Pacific region
Geared towards enhancing interoperability with Australia's key regional partners.  
Including:
- Fiji
- Tonga
- Samoa
- Vanuatu
- Solomon Islands

## SEAD 2018
South East Asia

## OCEAN EXPLORER 2018
East coast of Australia

## Ships, Boats, Craft 
`Requires completion`
### Current Ships & Boats by Designation
![logo](http://www.navy.gov.au/sites/default/files/LHD_AWD_ANZAC_FFG_Size_Comparison.jpg "Size Comparison")

**Amphibious Assault Ship (LHD)**  
Also known as a Landing Helicopter Dock  
- Embark, transport and deploy an embarked force along with equipment and aviation units
- Carry out/support humanitarian missions

**Destroyer, Guided Missile (DDG)**
- Provides air defence for accompanying ships & land forces & infrastructure in coastal areas

**Frigate, Guided Missile (FFG)**
- Long-range escort ship
- air defence
- anti-submarine warfare
- surveilance
- interdiction
- reconnaissance

**Frigate, Helicopter (FFH)**
- Can land the Seahawk
- Capable of air, surface & undersea warfare, surveillance & reconnaissance

**Landing Ship, Dock (LSD)**
- Large flight deck aft, can accommodate 2 Chinook's
- And at the stern cabable of operating landing craft

**Minehunter, Coastal (MHC)**
- Unique hull design, outstanding shock resistance & low magnetic signature to operate in hostile mine envirnonments

**Oiler Replenishment, Naval (AOR/OR)**
- Provides operational support for the rest of the fleet
	- fuel
	- stores
	- ammunition

**Patrol Boat, General (PB)**  
Primarily used in boarder protection in operation RESOLUTE & operation SECURE BOARDERS

**Submarine, Attack, Guided Missile (SSG)**

**Survey Ship (AGS)**

**Survey Ship, Coastal (AGSC)**

### Future Ships & Boats

**Arafura Class OPV**

**Attack Class SSG**

**Hunter Class FFG**

**Supply Class AOR**  
Planned to replace the OR 


### Locations

**ADFA** - Campbell, ACT  
The Australian Defenceforce Academy, develops & educates the future leaders of the Navy, Army & Air Force

**Fleet Base East** - Potts Point, NSW  
Home to numerous Anzac and Adelaide class Frigates, a Landing Ship, an offshore support vessel and the new Canberra Class Amphibious Ship

**HMAS Kuttabul** - Potts Point, NSW  
Overlooking Fleet Base East, provides administrative, training & logistics support to personnel within the Sydney area.

**HMAS Albatross** - South Nowra, NSW  
Navy largest operational establishment and home of the Fleet Air Arm, supporting Squirrel, Seahawk and MRH-90 helicopter squadrons, as well as aerial targets for anti-aircraft operations.

**HMAS Cairns** - Portsmith, QLD  
Provides logistic & administrative support to nermous specialist units, is home to patrol boats,
Hydrographic Survey vessels, Motor Survey Launches as well as Laser Airborne Depth Sounder aircraft.

**HMAS Cerberus** - Crib Point, VIC  
70km from Melbourne, Approx 6 000 personnel are trained annually.
Recruit School, School of Sea Survivability, Physical Training Instructor School,
Defence Force School of Signals and the Engineering Faculty

*To see more visit:* [link](https://navy.defencejobs.gov.au/about-the-navy/locations)

## Navy Basic Training - HMAS Cerberus
Situated on Western Port Bay, Victoria, approx. 70km SE of Melbourne.

First four days will be mainly taken up by medical & dental checks, issue of inital kit, a hair cut, completion of documentation, parade training & fitness training.

Navy recruit training continued over next 11 weeks. Designed to give you sufficient knowledge & skills to provide a basis for your Service career.

Although trainingdays can differ the following routine provides an indication of a typical day at Recruit School:

0520			Call the hands  
0525			Recruit School fall in for roll call  
0530 - 0600		Early Morning Activity (EMA) (not for Duty Watch)  
0550			Duty Watch out pipes -fall in on Forecastle  
0555 - 0630		Duty Watch march to breakfast  
0600 - 0620		EMA hands clean into dress of the day  
0620 - 0645		EMA hands fall in by classes  
0625 - 0645		EMA hands to breakfast  
0630			Duty Watch out pipes -muster  
0635 - 0715		Duty Watch to cleaning stations  
0645 - 0715		EMA hands to cleaning stations  
0730			Recruits attend Morning Parade or Instruction  
1115			Lunch  
1220			Fall in for 'Both Watches' (muster)  
1230 			Instruction  
1630			Secure from Instruction, cleaninto night clothing  
1700			Duty Watch muster, Recruit School fall in for evening meal parade  
1710			Recruit school march to dinner  
1715 - 1745		Dinner1830Cleaning Stations  
1900 - 2000		Duty Officer's Rounds (Inspection)  
2230			Lights Out  

**Phase 1:**  
*19 days (including weekends)* includes 'kit up', swim test, general induction (rites of passage), basic Navy knowledge and parade training.

**Phase 2:**  
Summative assessments 1 & 2, normally undertaken whilst conducting phase 3.

**Phase 3:**  
*12 day period* Required to be completed within 12 months from Enlistment. Dependant on availability, Phase 3 may be conducted immediately following Phase 1.  
Comprises of combat survivability (fire-fighting & damage control), survival at sea & first aid.

**Course Content**  
[navy.gov.au](http://www.navy.gov.au/join-navy/recruit-school)  
The course includes classroom and practical activities together with 5 days at sea on a training vessel.
- Living in Communal Harmony
- Maintenance of Service Kit
- Work Health and Safety
- Equity and Diversity
- Ceremonial
- Drill (Marching)
- Cleaning
- Physical Training
- Drugs and Alcohol Education
- Leave and Travel
- Steyr Rifle Training
- First Aid
- Survival at Sea

# Electronics Technician (ET)
[Link to info](https://navy.defencejobs.gov.au/jobs/electronics-technician)  
Work at the leading edge of technology maintaining advanced missile systems,
navigation equipment, radar, surveillance equipment, communication networks and more.

## About the Job

### Service Benefits
- Competitive salary
- Free medical & dental
- A healthy working life
- Job security & career development
- Advanced technology
- World-class education & training

## Key information
### National Qualifications
All eligible personnel will recieve a Cert III in Electronics & Communications.  
This qualification is nationally recognised.

### Salary & Allowances
**Starting salary:**
- Total: 				$50k
**Qualified:**
- Total: 				$81k

### Locations
All sailors may serve in fleet units & shore establishments on a
rotational basis in different geographical locations.

## Requirements
### Age
Age: 17 - 53 years inclusive of entry.

### Education & Expreience
Must have completed Yr 10 with passes in English, Mathematics & Science (preferably with Physics content).

### Medical & Fitness
**Physical fitness test:**
- 15 push ups
- 20 sit ups
- 6.1 shuttle run

### Security Clearance
Negative Vetting Level 1 (NV1)

**Royal Australian Navy Swim Test (RANST)**  
RANST is conducted to ensure the Navy's duty of care to all serving personnel.  
*The purpose of RANST is to ensure fundamental water survival skills as a prerequisite to training
such as survival at sea training.*

- A safety jump off a 3m tower in overalls
- A 10m underwater swim in overalls
- A 50m swim using three safety strokes
- The ability to tread water or float for 15mins in overalls

*Failure any one component will constitute failure of the entire RANST and no waivers will be granted.*

### Period of Service
**Initial Minimum Period of Service (IMPS):** 6 years.  
Upon enlistment if you elect to serve an open ended elistment,
you will be able to serve until retirement age (subject to suitability for service).  
Or fixed period of service, subsequent periods of service to the requirements of ADF and suitability for further service.

### Aptitude

### Security Requirements
Security clearance is critical to an applicant's successful progression.

## Training
### Military Training
- Designed to give you sufficient knowledge and skills on which to base your Navy career.
- Course includes both classroom and practical activites.
- Most subjects are individually tested.
- Examinations are set to ensure recruits are sufficiently prepared to undertake further specialist training at category schools.
- Private study areas are avaliable in the school & Duty instructor is availiable for assistance during non-instructional hours.

- Upon completion [Joining instuctions](http://content.defencejobs.gov.au/pdf/navy/DFN_Document_SailorJoiningInstructions.pdf)

### Employment Training

**Course:** Electronics Technician Initial Technical Training (ITT)  
**Duration: 40 weeks**  
**Location:** HMAS Cerberus, Crib Point, Victoria

- Participate in electronics and communications work and competency development activities
- Apply Occupational Health and Safety regulations, codes and practices in the workplace
- Fabricate, assemble and dismantle utilities industry components
- Solve problems in D.C. circuits
- Document and apply measures to control WHS risks associated with electrotechnology work
- Repair basic electronic apparatus faults by replacement of components
- Troubleshoot single phase input D.C. power supplies
- Troubleshoot digital subsystems
	- Use of voltage, current & resistance measuring devices
		- providing solutions derived from measurments & calculations to predictable problems
	- Analogue & digital signals
		- Observing digital & Analogue signals
	- Numbering systems & conversions
		- Binary, Hex, Dec
		- Binary addition & subtraction
			- Two's complement
		- Binary Coded Decimal (BCD)
		- Conversion between numbering systems
		- Gray Code
		- Unicode
	- Combinational logic circuits
		- Logic gates
		- Truth tables
	- Digital Displays
		- Seven segment LED displays
		- Current limiting
		- Multiplexed displays
		- Liquid Crystal Displays (LCD)
	- Digital subsystem building blocks
		- Encoders & Decoders
		- Multiplexers & Demultiplexers
		- Timing diagrams
		- Flip flips, Latches & registers
		- Clocks
- Troubleshoot amplifiers in an electronic apparatus
- Troubleshoot resonance circuits in an electronic apparatus
	- RLC Circuits
		- Complex Numbers
		- Applications as oscillator circuits, Radio receivers & television sets
		- High, Low & Band-pass filter pass filters
- Fault find and repair complex power supplies
- Troubleshoot basic amplifier circuits
	- Amplifies a signal
		- Asin(ωt+p) == Asin(2πf+p)
			- A = Amplitude
			- f = frequency (number of oscillations in a period (1sec))
			- ω = 2πf, rate of change in units of radians per second (rad/s)
			- p = (supposed to be greek **phi** char) phase (in rads),
				when p != 0 it appears to be time shifted by p/ω seconds (-ve = delay & +ve = advance)
- Solve fundamental electronic communications system problems
- Apply environmentally and sustainable energy procedures in the energy sector
- Use computer applications relevant to a workplace
- Comply with scheduled and preventative maintenance program processes
- Solve problems in ELV single path circuits
- Select electronic components for assembly
- Use lead-free soldering techniques
	- **Why use a lead-free solder?**
		- Green option
		- Lead-free solder flows less resulting in less electrical shorts, Therefore:
			- Boards have fewer faults/ problems
			- Able to reduce the size of solders 
			- Removes harmful lead from the workplace
- Assemble, set-up and test computing devices
- Use engineering applications software on personal computers
- Install and configure a client computer operating system and software
- Set up and configure basic local area network (LAN)
- Use drawings, diagrams, schedules, standards, codes and specifications
- Repair predictable faults in general electronic apparatus
- Fault find and repair electronic apparatus
- Solve oscillator problems


### Further Training

**Course:** Specialisation Training  
**Duration: 5 - 9 weeks dependent of specialisation**  
**Location:** HMAS Cerberus, Crib Point, Victoria  

# What you will need for recruit school
Bring to enlistment day:
- Pen & Notepad
- Finance details - you will need these to get paid.
- Bank account details - BSB/Account number/Branch name.
- Tax File Number - if you do not have a TFN, you need to apply for one before enlisting in the Navy.
- All the items in the 'What to bring' table, below.
- All the documentation as listed in the 'Document checklist', below.
- All the requirements as listed in 'Other important information', below.


**DO NOT under any circumstances bring the following items to RS:**
- Knives and fire arms or their replicas or other offensive weapons such as cross bows, spear guns and spears;
- Illegal drugs and their associated implements. Prescription drugs must be declared to Recruit School staff on arrival;
- CB and amateur radios;
- Lap top computers;
- Offensive or inappropriate material, such as pornographic magazines, posters, books, clothing, videos and DVDs;
- Skate boards, roller blades, scooters; and
- Electrical extension leads, power boards, double adaptors.

What to bring:
**You will be issued with a Navy tracksuit** which you will wear until you recieve the rest of your uniform during your training.
The following is suggested list of articles of clothing, toiletries and other items you must bring with you.

| **Item** | **Description** |
| Suitcase/bag		| MUST BE LOCKABLE to store civ clothing. RAN takes no responsibility for lost items. |
| Nightwear			| Two-piece pyjamas/nightie/boxers & t-shirt. Conservative styles. |
| Bathrobe\*		| Light weight/conservative style. |
| Rubber thongs		| Shower footwear - crocs/non-slip style shoes are suitable |
| Underwear			| Minimum of 4 pairs, must be white or flesh-coloured cotton |
| Running shoes 	| Appropriate sporting shoes |
| Shoe brush\*		| For polishing shoes & boots |
| Shoe polish\*		| One tin of Parade Gloss boot polish is recommended |
| Toiletries		| As required, allow 1 month |
| Washing powder \* | 1kg recommended |
| Steam iron \*		| Good quality will serve you well |

\* denotes items can be purchased at the Recruit School canteen during the first week, however, prior purchase is highly recommended.

A bag can be purchased for aprrox. $30:
- Shoe brush
- Shoe polish
- Parade Gloss
- Name label kit
- White marker
- Yellow polish cloth
- Coat hangers
- Starch

